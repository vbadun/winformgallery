﻿using BusinessLogic;
using DataAccess;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolView
{
    class Program
    {
        static void Main(string[] args)
        {
            
            //  var pict = new Picture { Name = "Vasa", Size = 1000, TimeOfCreate = DateTime.Now };
            ////  context.Pictures = new List<Picture>();
            // context.Add(pict);

            var pict = new Picture { Name = "Vasa", Size = 1000, TimeOfCreate = DateTime.Now };


            IKernel ninjectKernel = new StandardKernel();
            ninjectKernel.Bind<IRepository>().To<FileSystemRepository>();
            IRepository repo = ninjectKernel.Get<IRepository>();

                

            
            var pictures = repo.GetAllPictures();
            int i = 1;
            foreach (var item in pictures)
            {

                {
                    Console.WriteLine($"{i++}. { item.Name} \t { item.Size.ToString()}");

                }
                
            }
            Console.ReadLine();

           


        }
    }
}
