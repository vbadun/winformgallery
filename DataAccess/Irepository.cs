﻿using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
   public interface IRepository
    {
        void Add(Picture picture);
         IList<Picture> GetAllPictures();
        void Add(Genre picture);
        IList<Genre> GetAllGenre();
        void Delete(Picture picture);
        void SetPositiveRating(Picture picture);
        void SetNegativeRating(Picture picture);
    }
}
