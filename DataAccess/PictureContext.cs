﻿using BusinessLogic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class PictureContext
    {

        public IList<Picture> listpicture = new List<Picture>();
        public IList<Genre> listgenre = new List<Genre>();

        public IList<Picture> GetAllPictures()
        {

            return listpicture;
        }
        public IList<Genre> GetAllGenre()
        {

            return listgenre;
        }

        public void Add(Picture picture)
        {

            listpicture.Add(picture);
        }
        public void Add(Genre genre)
        {

            listgenre.Add(genre);
        }
        public void Delete(Picture picture)
        {

            listpicture.Remove(picture);
        }

        public void SetPositiveRating(Picture picture)
        {

            for (int i = 0; i < listpicture.Count; i++)
            {
                if (listpicture[i].Path == picture.Path)
                {
                    listpicture[i].Rating++;
                    return;
                }
            }

        }
        public void SetNegativeRating(Picture picture)
        {

            for (int i = 0; i < listpicture.Count; i++)
            {
                if (listpicture[i].Path == picture.Path)
                {
                    listpicture[i].Rating--;
                    return;
                }
            }

        }
    }

}
