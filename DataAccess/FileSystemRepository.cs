﻿using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public class FileSystemRepository : IRepository
    {


        private PictureContext pc = new PictureContext();
        public void Add(Picture picture)
        {
            pc.Add(picture);
        }

        public void Add(Genre genre)
        {
            pc.Add(genre);
        }

        public void Delete(Picture picture)
        {
            pc.Delete(picture);
        }

        public IList<Genre> GetAllGenre()
        {
            return pc.GetAllGenre();
        }

        public IList<Picture> GetAllPictures()
        {
            return pc.GetAllPictures();
        }

        public void SetNegativeRating(Picture picture)
        {
            pc.SetNegativeRating(picture);
        }

        public void SetPositiveRating(Picture picture)
        {
            pc.SetPositiveRating(picture);
        }
    }

}
