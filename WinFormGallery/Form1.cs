﻿

using BusinessLogic;
using DataAccess;
using Ninject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormGallery
{
    public partial class Form1 : Form
    {
        private readonly IRepository filesystemRepository;



        public Form1(IRepository filesystemRepository)
        {
            InitializeComponent();


            this.filesystemRepository = filesystemRepository;




        }



        public void UpdateList()
        {
            var listpictures = filesystemRepository.GetAllPictures().ToList();
            var listgenreis = filesystemRepository.GetAllGenre().ToList();

            listBox1.DataSource = listpictures;
            listBox1.DisplayMember = "Name";
            listBox1.ValueMember = "Id";

            listBox2.DataSource = listgenreis;
            listBox2.DisplayMember = "Name";
            listBox2.ValueMember = "Id";
        }

        private void button1_Click(object sender, EventArgs e)
        {

            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.Filter = "(*.BMP; *.JPG; *.GIF;*.JPEG)| *.BMP; *.JPG; *.GIF;*.JPEG ";
            openFileDialog1.Multiselect = true;

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                var files = openFileDialog1.FileNames;

                foreach (var file in files)
                {
                    var fi = new FileInfo(file);
                    var g = GetGenre();
                    var pict = new Picture
                    {
                        genre = g,
                        Id = Guid.NewGuid(),
                        Name = fi.Name,
                        Size = fi.Length,
                        Path = fi.FullName,
                        TimeOfCreate = DateTime.Now,
                        Rating = 0
                    };

                    filesystemRepository.Add(pict);
                }
            }
            UpdateList();
        }

        private Genre GetGenre()
        {

            var selectedgenre = (Genre)listBox2.SelectedItem;
            return selectedgenre;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var fi = new FileInfo("d:\\1.jpg");


            var genredefault = new Genre
            {
                Id = Guid.NewGuid(),
                Name = "Nature"
            };
            var pictdefault = new Picture
            {

                Id = Guid.NewGuid(),
                Name = fi.Name,
                Size = fi.Length,
                Path = fi.FullName,
                TimeOfCreate = fi.CreationTime,
                Rating = 0,
                genre = genredefault

            };

            filesystemRepository.Add(pictdefault);
            filesystemRepository.Add(genredefault);
            UpdateList();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var selectedname = (Picture)listBox1.SelectedItem;

            filesystemRepository.SetNegativeRating(selectedname);
            UpdateList();
        }

        private void Select(object sender, EventArgs e)
        {

            var selectedname = (Picture)listBox1.SelectedItem;

            if (selectedname == null)
            {
                MessageBox.Show("Список пуст!");

            }
            else
            {
                pictureBox1.Image = Image.FromFile(selectedname.Path);
                TextName.Text = selectedname.Name;
                TextSize.Text = selectedname.Size.ToString();
                TextDateOfLoad.Text = selectedname.TimeOfCreate.ToString();
                TextRaiting.Text = selectedname.Rating.ToString();
                textGuid.Text = selectedname.Id.ToString();
                TextGenre.Text = selectedname.genre.Name;

            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            var selectedname = (Picture)listBox1.SelectedItem;

            filesystemRepository.Delete(selectedname);
            UpdateList();
        }

        private void Size_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            var selectedname = (Picture)listBox1.SelectedItem;
            filesystemRepository.SetPositiveRating(selectedname);

            UpdateList();

        }

        private void button_genre_Click(object sender, EventArgs e)
        {
            var genre = new Genre
            {
                Id = Guid.NewGuid(),
                Name = TextBoxGenre.Text
            };
            filesystemRepository.Add(genre);
            TextBoxGenre.ResetText();

            UpdateList();
        }
        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {

            RadioButton radioButton = (RadioButton)sender;
            if (radioButton.Checked)
            {
                var s = radioButton.Name;
                var listpictures = filesystemRepository.GetAllPictures().ToList();
                var list = new List<Picture>();
                switch (s)
                {
                    case "PictName":
                        list = listpictures.OrderBy(x => x.Name).ToList();
                        break;
                    case "PictSize":
                        list = listpictures.OrderBy(x => x.Size).ToList();
                        break;
                    case "DateOfCreate":
                        list = listpictures.OrderBy(x => x.TimeOfCreate).ToList();
                        break;
                    case "PictRating":
                        list = list = listpictures.OrderBy(x => x.Rating).ToList();
                        break;
                    case "PictGenre":
                        list = listpictures.OrderBy(x => x.genre.Name).ToList();
                        break;
                    default:
                        break;
                }
                listBox1.DataSource = list;
                listBox1.Refresh();
            }
        }

        private void TextBoxGenre_TextChanged(object sender, EventArgs e)
        {

        }

        private void SearchTextChanged(object sender, EventArgs e)
        {
            var listpictures = filesystemRepository.GetAllPictures().ToList();
            var searchlist = new List<Picture>();
            foreach (var item in listpictures)
            {
                if (item.Name.Contains(searchBox.Text))

                    searchlist.Add(item);
            }
            listBox1.DataSource = searchlist;
            listBox1.DisplayMember = "Name";
            listBox1.ValueMember = "Id";
        
        }

        private void buttonSaveJson_Click(object sender, EventArgs e)
        {
            var listpictures = filesystemRepository.GetAllPictures();
            var listgenres = filesystemRepository.GetAllGenre();

            var formatterPicture = new DataContractJsonSerializer(typeof(List<Picture>));
            var formatterPictere = new DataContractJsonSerializer(typeof(List<Genre>));

            using (FileStream fs = new FileStream("D:\\picturs.json", FileMode.Create))
            {
                formatterPicture.WriteObject(fs, listpictures);
            }
            var formatterGenre = new DataContractJsonSerializer(typeof(List<Genre>));
            using (FileStream fs = new FileStream("D:\\genries.json", FileMode.Create))
            {
                formatterGenre.WriteObject(fs, listgenres);
            }
        }

        private void buttonLoadFromJson_Click(object sender, EventArgs e)
        {
            var formatter = new DataContractJsonSerializer(typeof(List<Picture>));
            using (FileStream fs = new FileStream("D:\\picturs.json", FileMode.OpenOrCreate))
            {
                List<Picture> listpictures = (List<Picture>)formatter.ReadObject( fs);

                foreach (Picture p in listpictures)
                {
                    filesystemRepository.Add(p);
                }
            }
            var formattergenre = new DataContractJsonSerializer(typeof(List<Genre>));
            using (FileStream fs = new FileStream("D:\\genries.json", FileMode.OpenOrCreate))
            {
                List<Genre> listgenries = (List<Genre>)formattergenre.ReadObject(fs);

                foreach (Genre g in listgenries)
                {
                    filesystemRepository.Add(g);
                }
            }


            Console.ReadLine();
            UpdateList();
        }
    }
}
