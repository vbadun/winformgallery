﻿namespace WinFormGallery
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.TextName = new System.Windows.Forms.TextBox();
            this.TextSize = new System.Windows.Forms.TextBox();
            this.TextDateOfLoad = new System.Windows.Forms.TextBox();
            this.TextRaiting = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.textGuid = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TextGenre = new System.Windows.Forms.TextBox();
            this.button_genre = new System.Windows.Forms.Button();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.TextBoxGenre = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.PictGenre = new System.Windows.Forms.RadioButton();
            this.PictName = new System.Windows.Forms.RadioButton();
            this.PictRating = new System.Windows.Forms.RadioButton();
            this.PictSize = new System.Windows.Forms.RadioButton();
            this.DateOfCreate = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.searchBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonSaveJson = new System.Windows.Forms.Button();
            this.buttonLoadFromJson = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(900, 27);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(198, 70);
            this.button1.TabIndex = 1;
            this.button1.Text = "Открыть";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(107, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(529, 455);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.WaitOnLoad = true;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(669, 66);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(225, 95);
            this.listBox1.TabIndex = 3;
            this.listBox1.SelectedValueChanged += new System.EventHandler(this.Select);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button2.Location = new System.Drawing.Point(310, 488);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(49, 51);
            this.button2.TabIndex = 4;
            this.button2.Text = "-";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(900, 103);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(198, 70);
            this.button3.TabIndex = 5;
            this.button3.Text = "Удалить";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // TextName
            // 
            this.TextName.Location = new System.Drawing.Point(125, 38);
            this.TextName.Name = "TextName";
            this.TextName.Size = new System.Drawing.Size(100, 20);
            this.TextName.TabIndex = 6;
            // 
            // TextSize
            // 
            this.TextSize.Location = new System.Drawing.Point(125, 66);
            this.TextSize.Name = "TextSize";
            this.TextSize.Size = new System.Drawing.Size(100, 20);
            this.TextSize.TabIndex = 7;
            this.TextSize.TextChanged += new System.EventHandler(this.Size_TextChanged);
            // 
            // TextDateOfLoad
            // 
            this.TextDateOfLoad.Location = new System.Drawing.Point(125, 91);
            this.TextDateOfLoad.Name = "TextDateOfLoad";
            this.TextDateOfLoad.Size = new System.Drawing.Size(100, 20);
            this.TextDateOfLoad.TabIndex = 8;
            // 
            // TextRaiting
            // 
            this.TextRaiting.Location = new System.Drawing.Point(125, 118);
            this.TextRaiting.Name = "TextRaiting";
            this.TextRaiting.Size = new System.Drawing.Size(100, 20);
            this.TextRaiting.TabIndex = 9;
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button4.Location = new System.Drawing.Point(381, 488);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(48, 51);
            this.button4.TabIndex = 10;
            this.button4.Text = "+";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // textGuid
            // 
            this.textGuid.Location = new System.Drawing.Point(107, 493);
            this.textGuid.Name = "textGuid";
            this.textGuid.Size = new System.Drawing.Size(197, 20);
            this.textGuid.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(205, 496);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Guid";
            // 
            // TextGenre
            // 
            this.TextGenre.Location = new System.Drawing.Point(125, 144);
            this.TextGenre.Name = "TextGenre";
            this.TextGenre.Size = new System.Drawing.Size(100, 20);
            this.TextGenre.TabIndex = 13;
            // 
            // button_genre
            // 
            this.button_genre.Location = new System.Drawing.Point(948, 216);
            this.button_genre.Name = "button_genre";
            this.button_genre.Size = new System.Drawing.Size(150, 23);
            this.button_genre.TabIndex = 14;
            this.button_genre.Text = "Добавить жанр";
            this.button_genre.UseVisualStyleBackColor = true;
            this.button_genre.Click += new System.EventHandler(this.button_genre_Click);
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(948, 285);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(150, 95);
            this.listBox2.TabIndex = 3;
            this.listBox2.SelectedValueChanged += new System.EventHandler(this.Select);
            // 
            // TextBoxGenre
            // 
            this.TextBoxGenre.Location = new System.Drawing.Point(948, 245);
            this.TextBoxGenre.Name = "TextBoxGenre";
            this.TextBoxGenre.Size = new System.Drawing.Size(150, 20);
            this.TextBoxGenre.TabIndex = 15;
            this.TextBoxGenre.TextChanged += new System.EventHandler(this.TextBoxGenre_TextChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.PictGenre);
            this.groupBox1.Controls.Add(this.PictName);
            this.groupBox1.Controls.Add(this.TextGenre);
            this.groupBox1.Controls.Add(this.PictRating);
            this.groupBox1.Controls.Add(this.PictSize);
            this.groupBox1.Controls.Add(this.DateOfCreate);
            this.groupBox1.Controls.Add(this.TextName);
            this.groupBox1.Controls.Add(this.TextRaiting);
            this.groupBox1.Controls.Add(this.TextSize);
            this.groupBox1.Controls.Add(this.TextDateOfLoad);
            this.groupBox1.Location = new System.Drawing.Point(669, 167);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(234, 199);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Информация о картинке";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Сортировать по:";
            // 
            // PictGenre
            // 
            this.PictGenre.AutoSize = true;
            this.PictGenre.Location = new System.Drawing.Point(20, 145);
            this.PictGenre.Name = "PictGenre";
            this.PictGenre.Size = new System.Drawing.Size(54, 17);
            this.PictGenre.TabIndex = 0;
            this.PictGenre.TabStop = true;
            this.PictGenre.Text = "Жанр";
            this.PictGenre.UseVisualStyleBackColor = true;
            this.PictGenre.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // PictName
            // 
            this.PictName.AutoSize = true;
            this.PictName.Location = new System.Drawing.Point(20, 41);
            this.PictName.Name = "PictName";
            this.PictName.Size = new System.Drawing.Size(47, 17);
            this.PictName.TabIndex = 0;
            this.PictName.TabStop = true;
            this.PictName.Text = "Имя";
            this.PictName.UseVisualStyleBackColor = true;
            this.PictName.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // PictRating
            // 
            this.PictRating.AutoSize = true;
            this.PictRating.Location = new System.Drawing.Point(20, 118);
            this.PictRating.Name = "PictRating";
            this.PictRating.Size = new System.Drawing.Size(66, 17);
            this.PictRating.TabIndex = 0;
            this.PictRating.TabStop = true;
            this.PictRating.Text = "Рейтинг";
            this.PictRating.UseVisualStyleBackColor = true;
            this.PictRating.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // PictSize
            // 
            this.PictSize.AutoSize = true;
            this.PictSize.Location = new System.Drawing.Point(20, 69);
            this.PictSize.Name = "PictSize";
            this.PictSize.Size = new System.Drawing.Size(64, 17);
            this.PictSize.TabIndex = 0;
            this.PictSize.TabStop = true;
            this.PictSize.Text = "Размер";
            this.PictSize.UseVisualStyleBackColor = true;
            this.PictSize.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // DateOfCreate
            // 
            this.DateOfCreate.AutoSize = true;
            this.DateOfCreate.Location = new System.Drawing.Point(20, 92);
            this.DateOfCreate.Name = "DateOfCreate";
            this.DateOfCreate.Size = new System.Drawing.Size(100, 17);
            this.DateOfCreate.TabIndex = 0;
            this.DateOfCreate.TabStop = true;
            this.DateOfCreate.Text = "Дата загрузки";
            this.DateOfCreate.UseVisualStyleBackColor = true;
            this.DateOfCreate.CheckedChanged += new System.EventHandler(this.radioButton_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(948, 269);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Жанры:";
            // 
            // searchBox
            // 
            this.searchBox.Location = new System.Drawing.Point(669, 40);
            this.searchBox.Name = "searchBox";
            this.searchBox.Size = new System.Drawing.Size(224, 20);
            this.searchBox.TabIndex = 19;
            this.searchBox.TextChanged += new System.EventHandler(this.SearchTextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(676, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Фильтр по имени:";
            // 
            // buttonSaveJson
            // 
            this.buttonSaveJson.Location = new System.Drawing.Point(980, 508);
            this.buttonSaveJson.Name = "buttonSaveJson";
            this.buttonSaveJson.Size = new System.Drawing.Size(121, 40);
            this.buttonSaveJson.TabIndex = 20;
            this.buttonSaveJson.Text = "Сохранить картинки";
            this.buttonSaveJson.UseVisualStyleBackColor = true;
            this.buttonSaveJson.Click += new System.EventHandler(this.buttonSaveJson_Click);
            // 
            // buttonLoadFromJson
            // 
            this.buttonLoadFromJson.Location = new System.Drawing.Point(980, 555);
            this.buttonLoadFromJson.Name = "buttonLoadFromJson";
            this.buttonLoadFromJson.Size = new System.Drawing.Size(118, 34);
            this.buttonLoadFromJson.TabIndex = 21;
            this.buttonLoadFromJson.Text = "Загрузить сохраненные";
            this.buttonLoadFromJson.UseVisualStyleBackColor = true;
            this.buttonLoadFromJson.Click += new System.EventHandler(this.buttonLoadFromJson_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1113, 638);
            this.Controls.Add(this.buttonLoadFromJson);
            this.Controls.Add(this.buttonSaveJson);
            this.Controls.Add(this.searchBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TextBoxGenre);
            this.Controls.Add(this.button_genre);
            this.Controls.Add(this.textGuid);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox TextName;
        private System.Windows.Forms.TextBox TextSize;
        private System.Windows.Forms.TextBox TextDateOfLoad;
        private System.Windows.Forms.TextBox TextRaiting;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textGuid;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TextGenre;
        private System.Windows.Forms.Button button_genre;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.TextBox TextBoxGenre;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton PictGenre;
        private System.Windows.Forms.RadioButton PictName;
        private System.Windows.Forms.RadioButton PictRating;
        private System.Windows.Forms.RadioButton PictSize;
        private System.Windows.Forms.RadioButton DateOfCreate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox searchBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonSaveJson;
        private System.Windows.Forms.Button buttonLoadFromJson;
    }
}

