﻿using DataAccess;
using SimpleInjector;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormGallery
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        /// 
        private static Container container;

        [STAThread]
        static void Main()
        {

           
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Bootstrap();
            Application.Run(container.GetInstance<Form1>());

            

        }
        private static void Bootstrap()
        {
            // Create the container as usual.
            container = new Container();

            // Register your types, for instance:
            container.Register<IRepository, FileSystemRepository>(Lifestyle.Singleton);
         //   container.Register<IUserContext, WinFormsUserContext>();
            container.Register<Form1>();
          

            // Optionally verify the container.
           //container.Verify();
        }
    }
}
