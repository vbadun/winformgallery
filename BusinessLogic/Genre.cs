﻿using System;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;

namespace BusinessLogic
{
    [DataContract]
    public class Genre
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public string Name { get; set; }
    }
}