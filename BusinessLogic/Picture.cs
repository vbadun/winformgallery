﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace BusinessLogic
{
    [DataContract]
    public class Picture
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Path { get; set; }
        [DataMember]
        public long Size { get; set; }
        [DataMember]
        public DateTime TimeOfCreate { get; set; }
        [DataMember]
        public int Rating { get; set; }
        [DataMember]
        public Genre genre { get; set; }

    }
}
